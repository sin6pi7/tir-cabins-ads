Node.js based project for showing ads in specific toilet rooms.


### How do I get server set up? ###

* go to /web directory
* run `npm install`
* start app with `node src/app.js`
* go to http://localhost:3000 to see active cabins


### How do I get client set up? ###

* extract /client directory on your device
* go to /client
* run the client: 'python copernicusToilet.py [server IP] [server port] [cabin ID]'
* press the button on your device to make your cabin earn more money from ads!
