function ClientManager () {
	var _clients = [];

	function updateClients(cabins) {
		if (_clients.length === 0) {
			return;
		}

		cabins.forEach(_sendAdToClients);
	}

	function _sendAdToClients(cabin, idx) {
		var clients = _clients[cabin.getId()],
			ad = cabin.getAd();

		if (clients == undefined) {
			return;
		}

		for (var i = 0; i < clients.length; i++) {
			clients[i].emit('ad:changed', {ad: ad});
		}
	}

	function addClient(client, id) {
		_clients[id] = _clients[id] || [];
		_clients[id].push(client);
	}

	function removeClient(client) {
		var idx;

		for (var i = 0; i < _clients.length; i++) {
			if (_clients[i] == undefined) {
				continue;
			}
			
			idx = _clients[i].indexOf(client);

			if (idx == -1) {
				continue;
			}

			_clients[i].splice(idx, 1);
			break;
		}
	}

	return {
		updateClients: updateClients,
		addClient: addClient,
		removeClient: removeClient
	}
}

module.exports = ClientManager;