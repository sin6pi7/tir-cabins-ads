function AdsService () {
	var _ads = [
		'/images/audi.jpg',
		'/images/bmw.jpg',
		'/images/vw.jpg',
		'/images/tesco.jpg'
	];

	function getAds(n) {
		var ads = [];

		if (n === undefined) {
			return _ads;
		}

		for (var i = 0; i < n; i++) {
			if (i >= _ads.length) {
				ads[i] = randomAd();
				continue;
			}
			ads[i] = _ads[i];
		};

		return ads;
	}

	function randomAd() {
		return _ads[Math.floor(Math.random() * _ads.length)];
	}

	return {
		getAds: getAds,
		randomAd: randomAd
	}
}

module.exports = AdsService;