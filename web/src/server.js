var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

function Server (cabinManager, clientManager) {
	var _cabinManager = cabinManager,
		_clientManager = clientManager;

	// set up jade as template engine
	app.set('views', __dirname + '/../views');
	app.set('view engine', 'jade');

	// serve static files
	app.use(express.static(__dirname + '/../public'));

	// homepage
	app.get('/', function (req, res) {
		res.render('index', {cabins: _cabinManager.getCabinsIds()});
	});

	// ad for specific cabin
	app.get('/cabin/:id', function (req, res) {
		var cabin = _cabinManager.getCabinWithId(req.params.id);
		if (cabin == undefined) {
			res.send("Cabin with id " + req.params.id + " does not exist!");
			return;
		}
		res.render('cabin', {cabinId: cabin.getId(), ad: cabin.getAd()});
	});

	// cabin has been entered
	app.post('/cabin/:id', function (req, res) {
		console.log("Entered cabin no. " + req.params.id);
		res.send("ok");
		_cabinManager.enteredCabinWithId(req.params.id);
	});

	// keep client up to date with ads
	io.on('connection', function(socket){
		socket.on('client:register', function(data) {
			console.log("client registered for cabin with id " + data.id);
			_clientManager.addClient(socket, data.id);
		});

		socket.on('disconnect', function () {
			console.log("client diconnected");
			_clientManager.removeClient(socket);
		})
	});

	function start(port) {
		http.listen(port, function(){
		  console.log('listening on *:' + port);
		});
	}

	return {
		start: start
	}
}

module.exports = Server;
