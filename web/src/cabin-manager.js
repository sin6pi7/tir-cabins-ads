var Cabin = require('./cabin');

function CabinManager (clientManager, adsService) {
	var _cabins = [],
		_clientManager = clientManager,
		_adsService = adsService;

	function enteredCabinWithId(id) {
		var idx,
			ads;

		idx = _indexOfCabinWithId(id);

		if (idx == -1) {
			_cabins.push(new Cabin(id));
		} else {
			_cabins[idx].updateEnterCount();	
		}

		_cabins = _cabins.sort(_compareCabins);

		ads = _adsService.getAds(_cabins.length);

		for (var i = 0; i < _cabins.length; i++) {
			_cabins[i].setAd(ads[i]);
		}

		_clientManager.updateClients(_cabins);
	}

	function getCabinWithId(id) {
		var idx = _indexOfCabinWithId(id);
		if (idx == -1) {
			return undefined;
		}
		return _cabins[idx];
	}

	function getCabinsIds() {
		return _cabins.map(_idOnly);
	}

	function _idOnly(cabin) {
		return cabin.getId();
	}

	function _indexOfCabinWithId(id) {
		for (var i = 0; i < _cabins.length; i++) {
			if (_cabins[i].getId() == id) return i;
		};

		return -1;
	}

	function _compareCabins(cab1, cab2) {
		if (cab1.getEnterCount() > cab2.getEnterCount()) return -1;
		if (cab1.getEnterCount() < cab2.getEnterCount()) return 1;
		return 0;
	}

	return {
		enteredCabinWithId: enteredCabinWithId,
		getCabinsIds: getCabinsIds,
		getCabinWithId: getCabinWithId
	}
}

module.exports = CabinManager;