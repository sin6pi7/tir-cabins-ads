var adsService = require('./ads-service')();
var clientManager = require('./client-manager')();
var cabinManager = require('./cabin-manager')(clientManager, adsService);
var server = require('./server')(cabinManager, clientManager);

server.start(3000);

