function Cabin(id) {
	var _id = id,
		_enterCount = 1,
		_ad = {};

	function getId() {
		return _id;
	}

	function updateEnterCount() {
		_enterCount++;
	}

	function getEnterCount() {
		return _enterCount;
	}

	function setAd(ad) {
		_ad = ad;
	}

	function getAd() {
		return _ad;
	}

	return {
		getId: getId,
		updateEnterCount: updateEnterCount,
		getEnterCount: getEnterCount,
		setAd: setAd,
		getAd: getAd
	}
}

module.exports = Cabin;