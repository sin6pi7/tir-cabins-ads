(function() {
	var socket = io();
	var adEl = document.getElementById('ad');

	socket.emit('client:register', {id: adEl.getAttribute('data-id')});
	socket.on('ad:changed', function (data) {
		console.log("Change!");
		adEl.src = data.ad;
	})
})();