#!/usr/bin/env python

import serial
import requests
from sys import argv

ser = serial.Serial('/dev/ttyS0', 38400, timeout=1)


serverIP = argv[1]
serverPort = argv[2]
toiletID = argv[3]


# ask for updates
ser.write(chr(128+16+1))

#check response
while True:
	response = ser.read(1)
	if len(response) > 0:
		if ord(response) == 128+64+2+1:
			print 'Sending POST request'
			r = requests.post('http://' + serverIP + ':' + serverPort + '/cabin/' + toiletID)
			print 'Request status: ' + r.text
			
			